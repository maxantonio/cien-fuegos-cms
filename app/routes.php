<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::model('blog', 'Blog');
Route::model('post', 'Post');

Route::get('profile/{id}', function($id)
{
    $user = User::where('id', $id)->first();
    return View::make('blog.index', ['user' => $user]);
});

Route::get('blog/{blog}', function(Blog $blog)
{
    $posts = $blog->posts()->paginate(10);
    $posts->getFactory()->setViewName('pagination::simple');
    return View::make('blog.single', ['blog' => $blog])->nest('content', 'blog.entries', compact('posts'));

    //return View::make('blog.single', ['blog' => $blog, 'posts' => $posts]);
});

Route::get('post/{post}', function(Post $post)
{
    $comments = $post->comments()->where('approved', '=', 1)->get();
    return View::make('post.single', ['post' => $post, 'comments' => $comments]);
});


Route::resource('/user', 'UserController');
Route::controller('/', 'HomeController');
