<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
     * Columnas: id, title, blog_id (para la relación uno-muchos), read_more (extracto de texto), comment_count (número de comentarios)
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('blog_id');
            $table->string('read_more');
            $table->text('content');
            $table->unsignedInteger('comment_count');
            $table->timestamps();
            $table->engine = 'MyISAM';
		});
        DB::statement('ALTER TABLE posts ADD FULLTEXT search(title, content)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
