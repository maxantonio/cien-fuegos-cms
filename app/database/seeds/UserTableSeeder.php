<?php


class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        $vader = new User;
        $vader->username = 'doctorv';
        $vader->email = 'darthv@deathstar.com';
        $vader->password = Hash::make('thedarkside');
        $vader->first_name = 'Darth';
        $vader->last_name = 'Vader';
        $vader->save();

        $blog = new Blog;
        $blog->title = 'Tragic Hero';
        $vader->blogs()->save($blog);

        $content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit.
		    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum.
		    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus,
		    mauris justo volutpat elit,
		    eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue,
		    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.';

        for ($i = 1; $i <= 20; $i++) {
            $post = new Post;
            $post->title = "Post no $i";
            $post->read_more = substr($content, 0, 120);
            $post->content = $content;
            $blog->posts()->save($post);

            $maxComments = mt_rand(3, 15);
            for ($j = 1; $j <= $maxComments; $j++) {
                $comment = new Comment;
                $comment->commenter = 'xyz';
                $comment->comment = substr($content, 0, 120);
                $comment->email = 'xyz@xmail.com';
                $comment->approved = 1;
                $post->comments()->save($comment);
                $post->increment('comment_count');
            }
        }

        $blog = new Blog;
        $blog->title = 'Absentee Father';
        $vader->blogs()->save($blog);

        $luke = new User;
        $luke->username = 'goodsidesoldier';
        $luke->email = 'lightwalker@rebels.com';
        $luke->password = Hash::make('hesnotmydad');
        $luke->first_name = 'Luke';
        $luke->last_name = 'Skywalker';
        $luke->save();

        $blog = new Blog;
        $blog->title = "The Son";
        $luke->blogs()->save($blog);

        $blog = new Blog;
        $blog->title = "The Return of the Jedis";
        $luke->blogs()->save($blog);

        $yoda = new User;
        $yoda->username = 'greendemon';
        $yoda->email = 'dancingsmallman@rebels.com';
        $yoda->password = Hash::make('yodaIam');
        $yoda->first_name = 'Yoda';
        $yoda->last_name = 'Unknown';
        $yoda->save();

        $blog = new Blog;
        $blog->title = "No Fear";
        $yoda->blogs()->save($blog);

        $blog = new Blog;
        $blog->title = "Master of the Force";
        $yoda->blogs()->save($blog);
    }
}