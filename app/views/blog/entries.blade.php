@foreach($posts as $post)
<h1><a href="#">{{ $post->title }}</a></h1>

<p class="lead">by <a href="index.php">Start Bootstrap</a>
</p>
<hr>
<p>
    <span class="fa fa-clock-o"></span> Posted on {{ $post->created_at->format('F d, Y h:ia') }} </p>
<hr>
<hr>
<p>{{ $post->read_more.' ...' }}</p>
<a class="btn btn-primary" href="{{ '/post/'.($post->id) }}">Read More <span class="fa fa-chevron-right"></span></a>

<hr>
@endforeach
{{$posts->links()}}

