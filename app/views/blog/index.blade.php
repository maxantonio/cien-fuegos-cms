@extends('layouts.master')

@section('title') Blogs @stop

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

    <h1><i class="fa fa-cog"></i> Blog Administration <a href="/logout" class="btn btn-default pull-right">Logout</a></h1>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
            <tr>
                <th>Title</th>
                <th>Date/Time Added</th>
            </tr>
            </thead>

            <tbody>
    @foreach ($user->blogs()->get() as $blog)
            <tr>
                <td><a href="{{ $blog->url() }}"> {{ $blog->title }}</a></td>
                <td>{{ $blog->created_at->format('F d, Y h:ia') }}</td>
            </tr>
    @endforeach
            </tbody>

        </table>
    </div>

    <a href="/user/create" class="btn btn-success">New Blog</a>

</div>

    @stop

