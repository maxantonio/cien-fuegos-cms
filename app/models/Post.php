<?php

class Post extends Eloquent {

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function blog()
    {
        return $this->belongsTo('Blog');
    }
}