<?php

class Blog extends Eloquent {

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function posts()
    {
        return $this->hasMany('Post');
    }

    public function url()
    {
        return "/blog/".$this->id;
    }

}