<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function roles()
    {
        return $this->belongsToMany('Role', 'user_roles');
    }

    public function isAdmin()
    {
        $roles = $this->roles()->toArray();
        return !empty($roles);
    }

    public function hasRole($check)
    {
        return in_array($check, array_fetch($this->roles()->toArray(), 'name'));
    }

    private function getIdInArray($array, $term)
    {
        foreach($array as $key => $value) {
            if ($value == $term) {
                return $key;
            }
        }
    }

    public function makeAdmin($title)
    {
        $assigned_roles = array();

        $roles = array_fetch(Role::all()->toArray(), 'name');

        switch($title) {
            case 'super_admin':
                $assigned_roles[] = $this->getIdInArray($roles, 'edit_customer');
                $assigned_roles[] = $this->getIdInArray($roles, 'delete_customer');
            case 'admin':
                $assigned_roles[] = $this->getIdInArray($roles, 'create_customer');
            case 'blogger':
                $assigned_roles[] = $this->getIdInArray($roles, 'create_blog');
                $assigned_roles[] = $this->getIdInArray($roles, 'delete_blog');
            default:
                throw new \Exception("The status entered does not exist");
        }

        $this->roles()->attach($assigned_roles);
    }

    public function getFullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function blogs()
    {
        return $this->hasMany('Blog');
    }
}
