Desarrollar una implementación básica de un cms utilizando el framework laravel.

Version 1.00 
Descripción y funcionalidad
 
Todo
- Implementación  plataforma simple para blogs
- Usar bootstrap 3 como framework de cms
- Crear un sistema de usuarios
- Todos los usuarios usan la misma plantilla de html
- usuarios tienen la capacidad de definir titulos y contenidos del post, utilizando cualquier librería wysiwyg
- Usuarios tienen la capacidad de modificar el contenido previamente creado
- Existe un panel simplificado de administrador global (PA)
- En el PA se ve la lista de los usuarios registrados
- En el PA se pueden modificar el contenido escrito por los usuarios

*los anteriores contaran con la correspondiente pueba (TEST) de la metodologia TDD
* el codigo estara documentado en la forma pertinente que simplifique las modificaciones para usuarios externos.